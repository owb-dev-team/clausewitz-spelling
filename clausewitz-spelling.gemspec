
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "clausewitz/spelling/version"

Gem::Specification.new do |spec|
  spec.name          = "clausewitz-spelling"
  spec.version       = Clausewitz::Spelling::VERSION
  spec.authors       = ["Will Chappell"]
  spec.email         = ["wtchappell@gmail.com"]

  spec.summary       = "Spellchecker tool for Clausewitz engine files"
  spec.homepage      = "http://github.com/wtchappell/clausewitz-spelling"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"
  spec.add_development_dependency "pry"

  spec.add_dependency "ffi-hunspell-wtchappell"
  spec.add_dependency "optimist"
  spec.add_dependency "colorize"
  spec.add_dependency "damerau-levenshtein"
  spec.add_dependency "pragmatic_tokenizer"
end
